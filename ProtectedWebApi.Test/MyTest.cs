﻿using Microsoft.Owin.Testing;
using NUnit.Framework;
using ProtectedWebApi.Test.Helpers;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ProtectedWebApi.Test
{
    [TestFixture]
    public class MyTest
    {
        [Test]
        public async Task TestController_AnonymousHasAccess()
        {
            using (var server = TestServer.Create<Startup>())
            {
                var response = await server.HttpClient.GetAsync("/api/anonymous");
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, "HTTP Status {0} received", (int)response.StatusCode);
            }
        }

        [Test]
        public async Task TestController_AnonymousNoAccess()
        {
            using (var server = TestServer.Create<Startup>())
            {
                var response = await server.HttpClient.GetAsync("/api/god");
                Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode, "HTTP Status {0} received", (int)response.StatusCode);
            }
        }

        [Test]
        public async Task TestController_UserWithRequiredRole()
        {
            using (var server = TestServer.Create<Startup>())
            {
                string token = TestJwtFormat.GenerateJwtAccessToken("support", new Claim("role", "god"));
                var response = await server.GetWithBearerAsync("/api/god", token);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, "HTTP Status {0} received", (int)response.StatusCode);
            }
        }

        [Test]
        public async Task TestController_UserWithoutRequiredRole()
        {
            using (var server = TestServer.Create<Startup>())
            {
                string token = TestJwtFormat.GenerateJwtAccessToken("support", new Claim("role", "jezus"));
                var response = await server.GetWithBearerAsync("/api/god", token);
                Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode, "HTTP Status {0} received", (int)response.StatusCode);
            }
        }
    }
}
