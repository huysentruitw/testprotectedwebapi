﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Microsoft.Owin.Testing
{
    public static class TestServerExtensions
    {
        public static async Task<HttpResponseMessage> GetWithBearerAsync(this TestServer server, string path, string token)
        {
            var request = server.CreateRequest(path);
            request.AddHeader("Authorization", "Bearer " + token);
            return await request.GetAsync();
        }
    }
}
