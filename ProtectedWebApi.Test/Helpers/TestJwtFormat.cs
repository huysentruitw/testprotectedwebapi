﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;

namespace ProtectedWebApi.Test.Helpers
{
    public class TestJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        public const string SignatureAlgorithm = "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256";
        public const string DigestAlgorithm = "http://www.w3.org/2001/04/xmlenc#sha256";
        public const string NameClaimType = "preferred_username";
        public const string RoleClaimType = "role";

        private const string audience = "all";
        private const string issuer = "localhost";
        private static readonly byte[] key = Encoding.UTF8.GetBytes("This.Is.Our.Secret");

        #region ISecureDataFormat implementation

        public string Protect(AuthenticationTicket ticket)
        {
            if (ticket == null)
            {
                throw new ArgumentNullException("ticket");
            }
            DateTime now = DateTime.UtcNow;
            DateTime expires = now.AddMinutes(10);
            var signingCredentials = new SigningCredentials(
                signingKey: new InMemorySymmetricSecurityKey(key),
                signatureAlgorithm: SignatureAlgorithm,
                digestAlgorithm: DigestAlgorithm);
            var token = new JwtSecurityToken(issuer, audience, ticket.Identity.Claims, now, expires, signingCredentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();
            var handler = new JwtSecurityTokenHandler();
            SecurityToken token;
            ClaimsPrincipal principal = handler.ValidateToken(protectedText, new TokenValidationParameters
                {
                    NameClaimType = NameClaimType,
                    RoleClaimType = RoleClaimType,
                    ValidAudience = audience,
                    ValidateAudience = true,
                    ValidIssuer = issuer,
                    ValidateIssuer = true,
                    IssuerSigningKey = new InMemorySymmetricSecurityKey(key)
                }, out token);
            return new AuthenticationTicket(principal.Identity as ClaimsIdentity, new AuthenticationProperties
                {
                    AllowRefresh = false,
                    IsPersistent = true,
                    IssuedUtc = token.ValidFrom,
                    ExpiresUtc = token.ValidTo
                });
        }

        #endregion

        public static string GenerateJwtAccessToken(string userName, params Claim[] additionalClaims)
        {
            var claims = new List<Claim>
            {
                new Claim(NameClaimType, userName)
            };
            claims.AddRange(additionalClaims);
            var identity = new ClaimsIdentity(claims, "", NameClaimType, RoleClaimType);
            var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
            return new TestJwtFormat().Protect(ticket);
        }
    }
}
