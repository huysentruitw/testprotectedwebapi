﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Owin;
using ProtectedWebApi.Test.Helpers;

[assembly: OwinStartup(typeof(ProtectedWebApi.Test.Startup))]

namespace ProtectedWebApi.Test
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions
                {
                    AuthenticationType = "TestAuth",
                    AuthenticationMode = AuthenticationMode.Active,
                    AccessTokenFormat = new TestJwtFormat()
                });
            app.UseProtectedWebApi();
        }
    }
}
