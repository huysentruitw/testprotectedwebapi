﻿using System.Web.Http;

namespace Owin
{
    public static class UseProtectedWebApiExtension
    {
        public static IAppBuilder UseProtectedWebApi(this IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            app.UseWebApi(config);
            return app;
        }
    }
}
