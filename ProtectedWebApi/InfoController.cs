﻿using System.Web.Http;

namespace TestProtectedWebApi
{
    [Authorize]
    [RoutePrefix("api")]
    public class InfoController : ApiController
    {
        [HttpGet]
        [Route("anonymous")]
        [AllowAnonymous]
        public IHttpActionResult Anonymous()
        {
            return Ok();
        }

        [HttpGet]
        [Route("god")]
        [Authorize(Roles = "god")]
        public IHttpActionResult God()
        {
            return Ok();
        }
    }
}